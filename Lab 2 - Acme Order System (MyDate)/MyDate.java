public class MyDate {
	/* instance variables are being initialized to be given non-NULL default values later.
	 * If they're given values both here and in the initialization block, then the values provided
	 * lower in the code will be used. If they're given values in the constructor, those values will
	 * ultimately be the ones used.
	 */
	int month;
	int day;
	int year;
	
	/* This initialization block ensures that the instance variables are given non-NULL default values
	 * even if no arguments are provided when creating a new MyDate object */
	{
		month = 1;
		day = 1;
		year = 2000;
	}
	
	/* A combination of no-argument and argument constructors allow for a more
	 * robust acceptance of varied inputs when creating an object */
	public MyDate() {}
	
	public MyDate(int m, int d, int y) {
		month = m;
		day = d;
		year = y;
	}
	
	/* The following methods are easy to tell apart from the above constructors because of their names
	 * that differ from the class name and file name in addition to the use of return types, which
	 * constructors do NOT have.
	 */
	public String toString() {
		String s = month + "/" + day + "/" + year;
		return s;
	}
	
	public void setDate(int m, int d, int y) {
		month = m;
		day = d;
		year = y;
	}
}
