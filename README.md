     ____                                                      __        ____                                         __                            
    /\  _`\                                        __         /\ \__    /\  _`\                                      /\ \__  __                     
    \ \,\L\_\  __  __    ___      __   _ __    __ /\_\    ____\ \ ,_\   \ \ \/\_\    ___     ___ ___   _____   __  __\ \ ,_\/\_\    ___      __     
     \/_\__ \ /\ \/\ \ /' _ `\  /'__`\/\`'__\/'_ `\/\ \  /',__\\ \ \/    \ \ \/_/_  / __`\ /' __` __`\/\ '__`\/\ \/\ \\ \ \/\/\ \ /' _ `\  /'_ `\   
       /\ \L\ \ \ \_\ \/\ \/\ \/\  __/\ \ \//\ \L\ \ \ \/\__, `\\ \ \_    \ \ \L\ \/\ \L\ \/\ \/\ \/\ \ \ \L\ \ \ \_\ \\ \ \_\ \ \/\ \/\ \/\ \L\ \  
       \ `\____\/`____ \ \_\ \_\ \____\\ \_\\ \____ \ \_\/\____/ \ \__\    \ \____/\ \____/\ \_\ \_\ \_\ \ ,__/\ \____/ \ \__\\ \_\ \_\ \_\ \____ \ 
        \/_____/`/___/> \/_/\/_/\/____/ \/_/ \/___L\ \/_/\/___/   \/__/     \/___/  \/___/  \/_/\/_/\/_/\ \ \/  \/___/   \/__/ \/_/\/_/\/_/\/___L\ \
                   /\___/                      /\____/                                                   \ \_\                               /\____/
                   \/__/                       \_/__/                                                     \/_/                               \_/__/ 
This repository will hold some of the labs I'm doing for the Java Udemy course.

I'll also place other miscellaneous code, notes, any attempted coding challenge solutions somewhere in here under an appropriate folder.

Cheers, hope you're having a good day. ~

*Nik*
